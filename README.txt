/**
 * Unique Node Title module for Drupal
 * Compatible with Drupal 7.x
 *
 * By River Delta India [http://riverdeltaindia.com/]
 */
 

INSTALLATION
------------

1. Copy the jquery_update directory to your sites/SITENAME/modules directory.

2. Enable the module at Administer >> Site building >> Modules.

INTRODUCTION
------------ 
 
The Unique Node Title module provides a way to require that title fields
of a node are unique.

This module adds additional options to the administration page for each
content type (i.e. admin/config/system/unique) for specifying which
content type must be unique.

And This module will add one extra field in content type that we have select
administration page settings.

This module also allows you to add duplicate content if you want,for that
you just need to uncheck/deselect checkbox during node addtion. 
